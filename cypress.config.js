const { defineConfig } = require('cypress')
const createBundler = require('@bahmutov/cypress-esbuild-preprocessor')
const { addCucumberPreprocessorPlugin } = require('@badeball/cypress-cucumber-preprocessor')
const { createEsbuildPlugin } = require('@badeball/cypress-cucumber-preprocessor/esbuild')
const gxVerifier = require('@gaia-x/json-web-signature-2020/dist/verifier/gaia-x-signature.verifier')

async function setupNodeEvents(on, config) {
  await addCucumberPreprocessorPlugin(on, config)

  on(
    'file:preprocessor',
    createBundler({
      plugins: [createEsbuildPlugin(config)]
    })
  )

  on('task', {
    async verifySignatureByGaia({ vc }) {
      // Using Gaia-X json-web-signature-2020
      const verifier = new gxVerifier.GaiaXSignatureVerifier()
      try {
        await verifier.verify(vc)
        return true
      } catch (error) {
        console.log('signature verification failed')

        return false
      }
    }
  })

  return config
}

module.exports = defineConfig({
  e2e: {
    supportFile: false,
    specPattern: 'cypress/e2e/features/**/*',
    setupNodeEvents
  }
})
