# Gaia-X  API Test
- Compliance API 
- Notary API

## Overview

The "Gaia-X Notary API test" suite is a sophisticated testing framework designed to validate the functionality of the Gaia-X Notary API. Leveraging the power of Cypress and Cucumber, this suite facilitates both behavior-driven development (BDD) and end-to-end (E2E) testing. It includes comprehensive tests for Compliance and Notary APIs including : EORI (Economic Operators Registration and Identification), LEI (Legal Entity Identifier), and VAT (Value Added Tax) validations, ensuring robust verification processes.

## The test suite covers the following :


- Compliance API testing
- Compliance response verification in multiple test cases.
- Notary health check ⚡
- Valid response with code 200 from the Notary for a given ID (VAT, LEI, EORI) ✅.
- The certificate chain is a trust anchor 📜.
- The public extracted via the DID belongs to the certificate chain 🔑→📜.
- The signature verification (JWS signature verification)✍️.

> **Note:** The signature verification is done via [Gaia-X json-web-signature-2020](https://gitlab.com/gaia-x/lab/json-web-signature-2020).

## Testing using the CI

It is possible to run a test on any provided notary implementation using Gitlab CI:

- Navigate to Build/Pipelines then to `Run Pipeline`: [New Pipeline](https://gitlab.com/gaia-x/lab/test-suites/legal-registration-number-notary-api-test/-/pipelines/new)
- Then specify in Variables the key: `NOTARY_BASE_URL` with the value of the notary to test (if kept empty, the job won't run)
- Then the job will be found in [Jobs](https://gitlab.com/gaia-x/lab/test-suites/legal-registration-number-notary-api-test/-/jobs)
- After the job is done, a report can be found as an _Artifact_ (right menu in the job) or in this URL with the right job id:
  https://gitlab.com/gaia-x/lab/test-suites/legal-registration-number-notary-api-test/-/jobs/[JOB_ID]/artifacts/browse

## Local Testing

### Prerequisites

> ⚠️ Requires nodeJS 18.

- If you are using [NVM](https://github.com/nvm-sh/nvm), you can do

```bash
nvm install
nvm use
```

- npm (Node Package Manager)

### Installation Instructions

1. **Clone the Repository:**

   git clone <repository-url>

2. **Navigate to the Project Directory:**

   cd gaia-x-notary-api-test

3. **Install Dependencies:**

   npm install

### Test Suite Structure

- **Feature Files:** Located in the `features` directory, these files define the various test scenarios in a language-agnostic manner, allowing for easy understanding and modification.
- **Step Definitions:** JavaScript files that map the BDD scenarios to executable code. These are crucial for the link between the written test cases in the feature files and the code that tests the application.
- **Common Steps:** The `common_steps.js` file contains reusable steps for multiple test scenarios, promoting code reuse and maintainability.

![Notary Test Structure](assets/diagram.png)

### Updating URL for the use by a clearing house

- **cypress.env.json:** Located in the `root` directory, the cypress.env.json contains the list of base URLs to be updated based on the clearing house. All you need is to update the value of the variable declared in the file in order to run the tests for a given clearing house.
  > **Note:** The environment variables stored, are stored with '/' at the end. Therefore, you just need to update it and include the slash '/' at the end. the endpoint name is supposed to remain unchanged for all clearing houses.

### Running Tests

Execute the following command to run the entire test suite:

```bash
npm run test
```
**- if you wish to run Compliance test only:**

```bash
npm run test:compliance
```

**- if you wish to run Notary test only:**

```bash
npm run test:notary
```

**- if you wish to run the EORI feature separately:**

```bash
npm run test:notary:eori
```

**- if you wish to run the LEI feature separately:**

```bash
npm run test:notary:lei
```

**- if you wish to run the vat feature separately:**

```bash
npm run test:notary:vatid
```

### HTML-Report

- **cucumber-report.html:** generated after each test run and located in the `root` directory, an HTML report, user-friendly that describes the test results after run. It contains useful data (appears in dropdown). for example, it logs the public key that was extracted from the DID and the one extracted from the certificate chain in PEM format.
