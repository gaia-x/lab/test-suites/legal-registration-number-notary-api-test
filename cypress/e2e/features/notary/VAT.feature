Feature: Notary - VAT Number

  Scenario Outline: Calling Notary for VAT validation
    When The notary API is running
    And I call Notary with a '<VATNumber>'
    And the number is '<numberStatus>'
    Then The API should '<verify>' that VAT Number
    And I should '<verifySignature>'
    And I should '<verifyCertificateChain>' that the certificate chain is a trust anchor
    And I should verify that the public key '<relevance>' to the certficate chain

    Examples:
      | VATNumber     | numberStatus | verify      | verifySignature        | verifyCertificateChain | relevance     |
      | FR87880851399 | valid        | validate    | validate_signature     | exists                 | belongs       |
      | FR87880851390 | invalid      | notvalidate | not_validate_signature | DoesNotExist           | DoesNotBelong |
      | ldfklnk       | invalid      | notvalidate | not_validate_signature | DoesNotExist           | DoesNotBelong |
