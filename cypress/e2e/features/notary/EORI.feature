Feature: Notary - EORI
  To verify the authenticity of EORI numbers
  As a user of the Notary API
  I want to be able to validate EORI numbers and check their associated certificate chain and signature

  Scenario Outline: Calling Notary for EORI code validation
    When The notary API is running
    And I call Notary with an EORI '<EORI>'
    And The EORI code is '<codeStatus>'
    Then The API should '<verify>' that EORI number
    And I should '<verifySignature>'
    And I should '<verifyCertificateChain>' that the certificate chain is a trust anchor
    And I should verify that the public key '<relevance>' to the certficate chain


    Examples:
      | EORI                 | codeStatus | verify      | verifySignature        | verifyCertificateChain | relevance     |
      | FR53740792600014     | valid      | validate    | validate_signature     | exists                 | belongs       |
      | 96950ZZz86GCAKPYJ703 | invalid    | notvalidate | not_validate_signature | DoesNotExist           | DoesNotBelong |
      | ldfklnk              | invalid    | notvalidate | not_validate_signature | DoesNotExist           | DoesNotBelong |
