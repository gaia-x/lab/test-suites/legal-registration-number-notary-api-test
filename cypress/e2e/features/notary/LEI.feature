Feature: Notary - LEI

  Scenario Outline: Calling Notary for LEI code validation
    When The notary API is running
    And I call Notary with an LEI '<LEIcode>'
    And the LEI code is '<codeStatus>'
    Then The API should '<verify>' that LEI Code
    And I should '<verifySignature>'
    And I should '<verifyCertificateChain>' that the certificate chain is a trust anchor
    And I should verify that the public key '<relevance>' to the certficate chain


    Examples:
      | LEIcode              | codeStatus | verify      | verifySignature        | verifyCertificateChain | relevance     |
      | 9695007586GCAKPYJ703 | valid      | validate    | validate_signature     | exists                 | belongs       |
      | 96950ZZz86GCAKPYJ000 | invalid    | notvalidate | not_validate_signature | DoesNotExist           | DoesNotBelong |
      | ldfklnk              | invalid    | notvalidate | not_validate_signature | DoesNotExist           | DoesNotBelong |
