Feature: Complaiance API Test
  Scenario Outline: Testing the complaiance API with a valid VP
  When I call the complaiance API and I use a correctly formatted Verifiable Presentation
  Then The API response should be valid 201
  Scenario Outline: verify that compliance does not accept an missing value in the legal participant 
  When I call the complaiance API with a VP that has a missing value in the legal participant
  Then The API response should be invalid 409
  And the missing legal participant value is in the response body
  Scenario Outline: verify that compliance does not accept an missing value in the legal registration 
  When I call the complaiance API with a VP that has a missing value in the legal registration
  Then The API response should be invalid 409
  And the missing legal registration value is in the response body
  Scenario Outline: verify that compliance does not accept an missing value in the terms and conditons  
  When I call the complaiance API with a VP that has a missing value in the terms and conditons
  Then The API response should be invalid 409
  And the missing terms and conditons value is in the response body
  Scenario Outline: verify that compliance does not accept an invalid Signature
  When I call the complaiance API and I use a Verfiable credintial that has an invalid signature
  Then The API response should be invalid 409
  And signature phrase should be in the response body







