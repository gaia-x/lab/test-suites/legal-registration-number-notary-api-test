import { When, Then, attach, After } from '@badeball/cypress-cucumber-preprocessor'
import jwkToPem from 'jwk-to-pem'
import forge from 'node-forge'

After(() => {
  // Clear some environment variables after each case run
  Cypress.env('notaryResponse', null)
  Cypress.env('jws', null)
  Cypress.env('responseWithoutProof', null)
  Cypress.env('extractedDID', null)
  Cypress.env('publicKeyPEM', null)
  Cypress.env('pemUrl', null)
  Cypress.env('certChain', null)
  Cypress.env('pemPublicKey2', null)
})

When('The notary API is running', () => {
  cy.request({
    method: 'GET',
    failOnStatusCode: false,
    url: Cypress.env('NOTARY_BASE_URL'),
    headers: {
      'Content-Type': 'application/json'
    },
    body: {}
  }).as('HealthResponse')

  cy.get('@HealthResponse').then(response => {
    if (response.status === 200) {
      console.log('Notary is healthy')
    } else {
      throw new Error('Notary API is not online')
    }
  })
})

Then('I should {string}', verifySignature => {
  const response = Cypress.env('notaryResponse')
  if (response && response.body && response.body.proof && response.status === 200) {
    // even if the scenario is forcing it, The goal of this if statement is not try to validate a signature if the response status code of the notary was not 200
    const jws = response.body.proof ? response.body.proof.jws : null

    if (verifySignature !== 'validate_signature') {
      expect(verifySignature).to.be.equal('not_validate_signature')
    } else {
      expect(Cypress.env('notaryResponse').status).to.be.equal(200) //make sure that the response is 200
      expect(response.body).to.exist
      // [1] Store jws globally
      Cypress.env('jws', jws)
      // [2] Create a deep copy of the response body and remove the proof property
      let responseWithoutProof = { ...response.body }
      if (responseWithoutProof.proof) {
        delete responseWithoutProof.proof // Remove the proof property
      }
      // Store the response without proof as an env variable to use it later.
      Cypress.env('responseWithoutProof', responseWithoutProof)

      // Extraction of the DID in order to resolve and then obtain the public
      const proof = response.body.proof
      const extractedDID = proof.verificationMethod

      // Storing the extracted DID in order to resolve it
      Cypress.env('extractedDID', extractedDID)

      // Resolve the DID through an HTTP request
      cy.request(Cypress.env('DID_RESOLVE_BASE_URL') + `${extractedDID}`).then(didResponse => {
        expect(didResponse.status).to.equal(200)
        // Obtain the public key and store it from the response
        const publicKeyJWK = didResponse.body.didDocument.verificationMethod[0].publicKeyJwk
        // Transform the obtained public key into PEM format in order to verify the signature
        const publicKeyPEM = jwkToPem(publicKeyJWK)
        Cypress.env('publicKeyPEM', publicKeyPEM)

        // Extract x5u (PEM URL) and store it in an environment variable
        const pemUrl = publicKeyJWK?.x5u
        if (pemUrl) {
          Cypress.env('pemUrl', pemUrl)
        } else {
          // Handle case where x5u property does not exist or is not found
          throw new Error('something went wrong, the x5u propery were not found !')
        }
      })

      // call Gaia-X library for signature verification
      cy.task('verifySignatureByGaia', { vc: Cypress.env('notaryResponse').body }).then(isValid => {
        if (isValid) {
          attach('Valid signature')
        } else {
          throw new Error('Signature validation failed')
        }
      })
    }
  } else {
    attach(
      'Signature validation was not even initiated ! This is -maybe- due to\n 1. The provided ID is not a valid one \n2. There is a problem in the response code \n3. Some issue in the response body '
    )
    expect(verifySignature).to.be.equal('not_validate_signature')
  }
})

Then('I should {string} that the certificate chain is a trust anchor', verifyCertificateChain => {
  const pemUrlAsString = `${Cypress.env('pemUrl')}`
  if (verifyCertificateChain === 'exists' && Cypress.env('notaryResponse').status === 200) {
    cy.request({
      method: 'POST',
      failOnStatusCode: false,
      url: Cypress.env('REGISTRY_CHAIN_FILE_URL'),
      headers: {
        'Content-Type': 'application/json'
      },
      body: {
        uri: pemUrlAsString
      }
    }).then(response => {
      Cypress.env('chainVerRsp', response)
      expect(response.status).to.equal(200)
      attach(
        'The certificate is a trust anchor, certificate URL : ' +
          Cypress.env('pemUrl') +
          '\n' +
          'Response body and Status Code: ' +
          JSON.stringify({
            body: Cypress.env('chainVerRsp').body,
            status: Cypress.env('chainVerRsp').status
          })
      )
    })
  } else {
    attach('=> There is no certifcate for an invalid number. \nThe Response of notary API was ' + Cypress.env('notaryResponse').status)
    expect(Cypress.env('notaryResponse').status).not.to.equal(200)
  }
})

Then('I should verify that the public key {string} to the certficate chain', relevance => {
  if (relevance === 'belongs') {
    // Get the certificate chain
    cy.request(Cypress.env('pemUrl')).then(response => {
      const certChain = response.body
      Cypress.env('certChain', certChain)

      expect(response.status).to.eq(200)
      if (typeof Cypress.env('certChain') === 'string') {
        //formatting the certificate
        const formattedCerts = Cypress.env('certChain')
          .split('-----END CERTIFICATE-----')
          .filter(cert => cert.trim() !== '')
          .map(cert => cert.trim() + '\n-----END CERTIFICATE-----')
          .join('\n')
        //extraction of the public key from the certificate using forge library and formatting it into PEM format
        const fogCertificate = forge.pki.certificateFromPem(formattedCerts)
        const pemPublicKey = forge.pki.publicKeyToPem(fogCertificate.publicKey)
        const pemPublicKey2 = pemPublicKey.replace(/\r\n/g, '\n')
        Cypress.env('pemPublicKey2', pemPublicKey2)

        //compare two public keys
        if (pemPublicKey2 === Cypress.env('publicKeyPEM')) {
          console.log('The public keys match.')
          expect(pemPublicKey2).to.equal(Cypress.env('publicKeyPEM'), 'Public keys do not match')
          attach(
            'Public extracted from the DID document matches the one in the certificate chain' +
              '\nPublic key in the chain\n' +
              Cypress.env('pemPublicKey2') +
              '\nPublic key extracted from DID document\n' +
              Cypress.env('publicKeyPEM')
          )
        } else {
          console.log('The public keys do not match.')
          // assertion to automatically fail the test in case of a mismatch
          expect(pemPublicKey2).to.equal(Cypress.env('publicKeyPEM'), 'Public keys do not match')
        }
      }
    })
  } else {
    console.log('The ID provided does not constitute a valid number, therefore no verification conducted')
    attach('The ID provided does not constitute a valid number, therefore no verification conducted')
  }
})
