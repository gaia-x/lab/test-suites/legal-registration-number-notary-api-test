import { When, Then } from '@badeball/cypress-cucumber-preprocessor'

let vatNumber

When('I call Notary with a {string}', vatNum => {
  vatNumber = vatNum
  cy.request({
    method: 'POST',
    failOnStatusCode: false,
    url: Cypress.env('NOTARY_BASE_URL') + '/registrationNumberVC',

    headers: {
      'Content-Type': 'application/json'
    },
    body: {
      '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant'],
      type: 'gx:legalRegistrationNumber',
      id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
      'gx:vatID': vatNumber
    }
  }).then(response => {
    Cypress.env('notaryResponse', response)
  })
})

When('the number is {string}', stat => {
  const response = Cypress.env('notaryResponse')

  if (stat === 'valid') {
    if (response.body && response.body.credentialSubject) {
      expect(response.status).to.equal(200) // double check response status and fail test if not 200
    } else {
      attach('invalid vat number provided')
      expect(response.status).not.to.equal(200) // ensure that the response is not 200 in the ELSE statement otherwise test fails.
    }
  }
})

Then('The API should {string} that VAT Number', verification => {
  const response = Cypress.env('notaryResponse')

  if (verification === 'validate') {
    if (response.body && response.body.credentialSubject) {
      expect(response.status).to.equal(200)
      expect(response.body.credentialSubject['gx:vatID']).to.equal(vatNumber)
    } else {
      console.log('a ' + stat + ' has been provided')
    }
  }
})
