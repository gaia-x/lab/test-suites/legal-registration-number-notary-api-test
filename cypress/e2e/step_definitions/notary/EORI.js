import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
let EORI_value
let numberStatus

When('I call Notary with an EORI {string}', EORI_of_example => {
  EORI_value = EORI_of_example
  cy.request({
    method: 'POST',
    failOnStatusCode: false,
    url: Cypress.env('NOTARY_BASE_URL') + 'registrationNumberVC',
    headers: {
      'Content-Type': 'application/json'
    },

    body: {
      '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant'],
      type: 'gx:legalRegistrationNumber',
      id: 'did:web:gaia-x.eu:legalRegistrationNumber.json',
      'gx:EORI': EORI_value
    }
  }).then(response => {
    Cypress.env('notaryResponse', response)
  })
})

When('The EORI code is {string}', stat => {
  const response = Cypress.env('notaryResponse')
  numberStatus = stat

  if (stat === 'valid') {
    if (response.body && response.body.credentialSubject) {
      expect(response.status).to.equal(200) // double check response status and fail test if not 200
    } else {
      expect(response.status).not.to.equal(200) // ensure that the response is not 200 in the ELSE statement otherwise test fails.
    }
  }
})

Then('The API should {string} that EORI number', verification => {
  const response = Cypress.env('notaryResponse')
  if (verification === 'validate') {
    if (response.body && response.body.credentialSubject) {
      expect(response.status).to.equal(200)
      expect(response.body.credentialSubject['gx:EORI']).to.equal(EORI_value)
    } else {
      throw new Error('Invalid response structure for the invalid EORI_value Code' + response.body)
    }
  }
})
