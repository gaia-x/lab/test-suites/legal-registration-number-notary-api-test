import { When, Then, After } from '@badeball/cypress-cucumber-preprocessor'

beforeEach(function () {
  cy.fixture('VP-valid.json').as('VP_ok')
  cy.fixture('VP-misingLegalAddress-LegalParticipant.json').as('VP-misingLegalAddress-LegalParticipant')
  cy.fixture('VP-misingVATID-legalRegNo.json').as('VP-misingVATID-legalRegNo')
  cy.fixture('VP-misingTermsAndCond-TermsAndCond.json').as('VP_missing_terms_conditions')
  cy.fixture('VP-invalid-signature.json').as('VP-invalid-signature')
})

After(() => {
  Cypress.env('ComplianceResponse', null)
})

function complianceCall(request) {
  cy.request({
    method: 'POST',
    failOnStatusCode: false,
    url: Cypress.env('COMPLIANCE_BASE_URL'),
    headers: {
      'Content-Type': 'application/json'
    },
    body: request
  }).then(response => {
    if (response) {
      Cypress.env('ComplianceResponse', response)
    } else {
      throw new Error(`Request was not successful`)
    }
  })
}

When('I call the complaiance API and I use a correctly formatted Verifiable Presentation', function () {
  complianceCall(this.VP_ok.payload)
})

When('I call the complaiance API with a VP that has a missing value in the legal participant', function () {
  complianceCall(this['VP-misingLegalAddress-LegalParticipant'].payload)
})

When('I call the complaiance API with a VP that has a missing value in the legal registration', function () {
  complianceCall(this['VP-misingVATID-legalRegNo'].payload)
})

When('I call the complaiance API with a VP that has a missing value in the terms and conditons', function () {
  complianceCall(this['VP_missing_terms_conditions'].payload)
})

When('I call the complaiance API and I use a Verfiable credintial that has an invalid signature', function () {
  complianceCall(this['VP-invalid-signature'].payload)
})

Then('the missing legal participant value is in the response body', function () {
  const complianceResponse = JSON.parse(JSON.stringify(Cypress.env('ComplianceResponse')))
  const messagesAsString = complianceResponse.body.message.results?.join(' ')
  expect(messagesAsString).to.include('#legalAddress')
})

Then('the missing legal registration value is in the response body', function () {
  const complianceResponse = JSON.parse(JSON.stringify(Cypress.env('ComplianceResponse')))
  const messages = complianceResponse.body.message.results
  const messagesAsString = messages.join(' ')
  expect(messagesAsString).to.include('At least one of taxID, vatID, EUID, EORI or leiCode must be defined')
})

Then('the missing terms and conditons value is in the response body', function () {
  const complianceResponse = JSON.parse(JSON.stringify(Cypress.env('ComplianceResponse')))
  const messages = complianceResponse.body.message.results
  const messagesAsString = messages.join(' ')
  expect(messagesAsString).to.include('Missing expected value The PARTICIPANT signing the Self-Description')
})

Then('signature phrase should be in the response body', function () {
  const responseBody = Cypress.env('ComplianceResponse').body
  expect(responseBody.message).to.include('The provided signature does not match for')
})

Then('The API response should be valid 201', function () {
  expect(Cypress.env('ComplianceResponse').status).to.eq(201)
})

Then('The API response should be invalid 409', function () {
  expect(Cypress.env('ComplianceResponse').status).to.eq(409)
})
